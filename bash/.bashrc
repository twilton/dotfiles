# -----------------------------------------------------------------------------
# Description: Config file for non-login bash shell
# Location: $HOME/.bashrc
# -----------------------------------------------------------------------------

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Options {{{
# -----------------------------------------------------------------------------
# use extended pattern matching
shopt -s extglob
# check window size after each command
shopt -s checkwinsize

# save multiline cmd as one history entry
shopt -s cmdhist
# Only set histfile if XDG_DATA_HOME is defined
if [[ -n "$XDG_DATA_HOME" ]]; then
	if [[ -w "$XDG_DATA_HOME" ]] && [[ ! -d "$XDG_DATA_HOME/bash" ]]; then
		mkdir "$XDG_DATA_HOME/bash"
	fi

	HISTFILE="$XDG_DATA_HOME/bash/history"
fi
# increase history size
HISTSIZE=10000

# Help gpg display pinentry on correct tty
export GPG_TTY=$(tty)
# -----------------------------------------------------------------------------
# }}}

# Prompt {{{
# -----------------------------------------------------------------------------
__rc_prompt_git() {
	# check for git
	if ! type -p git &>/dev/null; then return; fi
	# check for repo
	if ! git rev-parse --is-inside-work-tree &>/dev/null; then return; fi

	# force git output in English
	local -r git_eng="env LANG=C git"
	# current branch name or short SHA1 hash for detached head
	printf ':%s\n' "$($git_eng symbolic-ref --short HEAD 2>/dev/null ||
		$git_eng describe --tags --always 2>/dev/null)"
}

__rc_prompt_build() {
	# Set color of EUID by exit code of last command
	if [[ "$?" -eq '0' ]]; then
		local privilege="\[\e[48;5;15m\]\\$\[\e[m\]"
	else
		local privilege="\[\e[48;5;1m\]\\$\[\e[m\]"
	fi

	# Sync history between terminals
	history -a; history -c; history -r;

	# pwd:git_branch \$
	PS1="\[\e[48;5;7m\] \[\e[38;5;0m\]\\w\[\e[38;5;8m\]$(__rc_prompt_git) \[\e[m\]${privilege} "
	# > \$
	PS2="\[\e[48;5;7m\] \[\e[38;5;7m\]> \[\e[m\]${privilege} "

	# Find length of pwd - $HOME + 1 (~)
	local -r dir_length="$(( ${#PWD} - ${#HOME} + 1 ))"
	# Trim displayed directories if pwd is too long
	if [[ "${dir_length}" -gt '24' ]]; then
		PROMPT_DIRTRIM=2
	elif [[ "${dir_length}" -gt '16' ]]; then
		PROMPT_DIRTRIM=3
	else
		PROMPT_DIRTRIM=0
	fi
}

# Set the prompts
PROMPT_COMMAND="__rc_prompt_build"
# -----------------------------------------------------------------------------
# }}}

# Aliases {{{
# Prompt before -r or > 3 files
alias rm='rm -vI'
# Prompt before overwrite
alias cp='cp -vi'
# Prompt before overwrite
alias mv='mv -vi'

# ls in nocolor, directory first, human readable, and entry classified mode
alias ls='ls --color=never --group-directories-first -hF'

# vi quit
alias :q='exit'

# Faster cd ..
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# file size information
alias du='du -h -d 1 | sort -hr'
# free space
alias df='df -h'

# Define google as default search elvi if surfraw is available
if type -p surfraw &>/dev/null; then
	alias s='surfraw google'
fi
# }}}

# XDG Support {{{
# XDG support for XDG noncompliant programs without environment variable
#   defined file locations
# Defined: tmux
# TODO: Remove once tmux 3.1 is out
if [[ -n "$XDG_CONFIG_HOME" ]]; then
	# Check that tmux is installed
	# Change config file to XDG if readable, otherwise tmux defaults to
	#   $HOME/.tmux.conf
	if [[ -x "$(which tmux 2> /dev/null)" ]]; then
		tmux() {
			# Check XDG config file to be used with -f is readable, or notify
			#   XDG config is not readable
			if [[ -r "$XDG_CONFIG_HOME/tmux/tmux.conf" ]]; then
				local -r config="$XDG_CONFIG_HOME/tmux/tmux.conf"
			fi

			# use defined XDG values or substitute default if unset
			command tmux -f "${config:-$HOME/.tmux.conf}" "$@"
		}
	fi
fi
# }}}

# Defaults {{{
# make fd behave more like git grep
if [[ -x "$(which fd 2> /dev/null)" ]]; then
	fd() {
		fd_default() {
			command fd \
				--follow \
				--color 'never' \
				"$@"
		}

		# if output is long, page it
		if [[ -t 1 ]]; then
			fd_default "$@" | less -RFX
		else
			fd_default "$@"
		fi

		#fd_default is only used by fd()
		unset -f fd_default
	}
fi

# make ripgrep behave more like git grep
if [[ -x "$(which rg 2> /dev/null)" ]]; then
	rg() {
		# set default rg colors
		rg_default() {
			command rg \
				--colors 'path:none' \
				--colors 'match:none' \
				--colors 'line:none' \
				--colors 'column:none' \
				--colors 'match:bg:2' \
				--colors 'path:bg:7' \
				-p -S "$@"
		}

		# if output is long, page it
		if [[ -t 1 ]]; then
			rg_default "$@" | less -RFX
		else
			rg_default "$@"
		fi

		# rg_default is only used by rg()
		unset -f rg_default
	}
fi
# }}}

# Plugins {{{
# -----------------------------------------------------------------------------
# Completion
if [[ -n "$PS1" ]]; then
	# use bash completion if available
	if [[ -r '/usr/share/bash-completion/bash_completion' ]]; then
		. '/usr/share/bash-completion/bash_completion'
	fi

	# use local bin completion if available
	if [[ -r "$HOME/.local/bin/bin_completion.bash" ]]; then
		. "$HOME/.local/bin/bin_completion.bash"
	fi
fi
# -----------------------------------------------------------------------------
# }}}
