# -----------------------------------------------------------------------------
# Description: Config file for bash login shell
# Location: $HOME/.bash_profile
# -----------------------------------------------------------------------------

# XDG {{{
# Base Directories {{{
# http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
#   XDG_RUNTIME_DIR is set by pam_systemd
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_CONFIG_DIRS="/etc/xdg"
# }}}

# XDG_RUNTIME_DIR ENV Var {{{
if [[ -n "$XDG_RUNTIME_DIR" ]]; then
	# tmux
	if type -p tmux &>/dev/null; then
		export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
	fi
fi
# }}}

# XDG_CONFIG_HOME ENV Var {{{
if [[ -n "$XDG_CONFIG_HOME" ]]; then
	# readline
	export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"

	# gpg
	if type -p gpg &>/dev/null; then
		export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
	fi

	# less
	if type -p less &>/dev/null; then
		# set less key config
		export LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
	fi

	# wget
	if type -p wget &>/dev/null; then
		if [[ -w "$XDG_CONFIG_HOME" ]]; then
			# mkdir $XDG_DATA_HOME/wget if it doesnt exist
			if [[ ! -d "$XDG_CONFIG_HOME/wget" ]]; then
				mkdir "$XDG_CONFIG_HOME/wget"
			fi
			# touch $XDG_DATA_HOME/wget/wgetrc if it doesnt exist
			if [[ -w "$XDG_CONFIG_HOME/wget" ]] &&
				[[ ! -f "$XDG_CONFIG_HOME/wget/wgetrc" ]]; then
					>"$XDG_CONFIG_HOME/wget/wgetrc"
			fi
		fi

		# TODO: check for wgetrc file
		export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
	fi

	# X
	if type -p X &>/dev/null; then
		export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
		export XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc"
		export XRESOURCES="$XDG_CONFIG_HOME/X11/xresources"
	fi
fi
# }}}

# XDG_CACHE_HOME ENV Var {{{
if [[ -n "$XDG_CACHE_HOME" ]]; then
	# less
	if type -p less &>/dev/null; then
		if [[ -w "$XDG_CACHE_HOME" ]] && [[ ! -d "$XDG_CACHE_HOME/less" ]]; then
			mkdir "$XDG_CACHE_HOME/less"
		fi

		# set less history file
		export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
	fi
fi
# }}}

# XDG_DATA_HOME ENV Var {{{
if [[ -n "$XDG_DATA_HOME" ]]; then
	# rust
	if type -p cargo &>/dev/null; then
		export CARGO_HOME="$XDG_DATA_HOME/cargo"
	fi

	# openssl
	if type -p openssl &>/dev/null; then
		export RANDFILE="$XDG_DATA_HOME/openssl/.rnd"
	fi

	# pass
	if type -p pass &>/dev/null; then
		export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
	fi
fi
# }}}
# }}}

# Paths {{{
# Add local bin to path
# TODO: check if $HOME/.local/bin exists
export PATH="$HOME/.local/bin:$PATH"
# }}}

# Encoding {{{
# UTF-8
export LC_ALL='en_US.UTF-8'
export LANG='en_US.UTF-8'
export LANGUAGE='en_US.UTF-8'
# }}}

# Programs {{{
# Defaults {{{
# Browser
if type -p firefox &>/dev/null; then
	export BROWSER='firefox'
elif type -p firefox-nightly &>/dev/null; then
	export BROWSER='firefox-nightly'
fi

# Editor
if type -p nvim &>/dev/null; then
	export EDITOR='nvim'
	export VISUAL="$EDITOR"
elif type -p vim &>/dev/null; then
	export EDITOR='vim'
	export VISUAL="$EDITOR"
fi

# Terminal
if type -p st &>/dev/null; then
	export TERMINAL='st'
elif type -p xterm &>/dev/null; then
	export TERMINAL='xterm'
fi
# }}}

# Options {{{
# GnuPG
if type -p gpg &>/dev/null; then
	# Forward ssh-agent to gpg-agent
	export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi

# gcc
if type -p gcc &>/dev/null; then
	# colored gcc output
	export GCC_COLORS="$(cat <<-GCC_COLOR_END
		error=41:warning=43:note=90:range1=30:range2=30:
		locus=46:quote=46:fixit-insert=46:fixit-delete=41:
		diff-filename=46:diff-hunk=30:diff-delete=41:diff-insert=46:
		type-diff=46
	GCC_COLOR_END
	)"
fi

# less
if type -p less &>/dev/null; then
	export LESS='-F -i -M -R -S -w -X -z-4'

	# colored man pages
	# begin bold
	export LESS_TERMCAP_mb=$'\E[0m'
	# begin blink
	export LESS_TERMCAP_md=$'\E[0m'
	# begin reverse video
	export LESS_TERMCAP_so=$'\E[47;39m'
	# begin underline
	export LESS_TERMCAP_us=$'\E[0m'
	# reset bold/blink
	export LESS_TERMCAP_me=$'\E[0m'
	# reset reverse video
	export LESS_TERMCAP_se=$'\E[0m'
	# reset underline
	export LESS_TERMCAP_ue=$'\E[0m'
fi

# ls
if type -p ls &>/dev/null; then
	# FILE:DIR:LINK:ORPHAN:MISSING:FIFO:SOCK:BLK:CHR:EXEC
	export LS_COLORS='fi=0:di=0:ln=0:or=90:mi=90:pi=0:so=0:bd=0:cd=0:ex=0'
fi

# grep
if type -p grep &>/dev/null; then
	# text selected line:text context line:whole selected line:whole context
	#   line:filename prefix:line number prefix:byte offset prefix:separator
	# Default: ms=01;31:mc=01;31:sl=:cx=:fn=35:ln=32:bn=32:se=36
	export GREP_COLORS='ms=0;30;42:mc=:sl=:cx=:fn=0;30;44:ln=:bn=:se='
fi
# }}}
# }}}

# Autostart {{{
# source .bashrc
if [[ -n "$BASH" ]] && [[ -r "$HOME/.bashrc" ]]; then
	. "$HOME/.bashrc"
fi

# X {{{
if [[ -z "$DISPLAY" ]] && [[ -n "$XDG_VTNR" ]]; then
	# unset SESSION_MANAGER
	unset SESSION_MANAGER
	# set display
	export DISPLAY=":$XDG_VTNR"

	# X clients
	defaultclient='xterm'
	userclientrc="${XINITRC:-$HOME/.xinitrc}"
	sysclientrc='/etc/X11/xinit/xinitrc'

	# X servers
	defaultserver='/usr/bin/X'
	userserverrc="${XSERVERRC:-$HOME/.xserverrc}"
	sysserverrc='/etc/X11/xinit/xserverrc'

	# set client
	if [[ -f "$userclientrc" ]]; then
		client="$userclientrc"
	elif [[ -f "$sysclientrc" ]]; then
		client="$sysclientrc"
	else
		client="$defaultclient"
	fi
	unset defaultclient userclientrc sysclientrc

	# set server
	if [[ -f "$userserverrc" ]]; then
		server="$userserverrc"
	elif [[ -f "$sysserverrc" ]]; then
		server="$sysserverrc"
	else
		server="$defaultserver"
	fi
	unset defaultserver userserverrc sysserverrc

	# xinit
	exec xinit "$client" -- "$server"

	# store xinit return value
	retval="$?"
	# free unused virtual terminals
	if type -p deallocvt &>/dev/null; then
		deallocvt
	fi
	# exit with xinit return value
	exit "$retval"
fi
# }}}
# }}}
