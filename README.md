# Dotfiles
|               |                                                            |
| ------------- | ---------------------------------------------------------- |
| **OS**        | [arch][info_os]                                            |
| **Shell**     | [bash][info_shell]                                         |
| **WM**        | [bspwm][info_wm]                                           |
| **Terminal**  | [st][info_term]                                            |
| **Font**      | [source code pro][info_font] & [freetype2][font_rendering] |
| **Colors**	| [uswds][info_colors]                                       |

# Install
*current release: `2020-04-05`*

This repo is managed with [GNU stow][info_stow]. **Backup** your dotfiles!

```bash
git clone git://gitlab.com/twilton/dotfiles ~/.dotfiles
cd ~/.dotfiles
stow <application>
```

# Preview

![Screenshot](https://gitlab.com/twilton/dotfiles/raw/master/preview.png)

# Inspiration

Dots I have shamelessly stolen from:
* [ajh17](https://github.com/ajh17/dotfiles)
* [cbarox](https://github.com/cbarox/Dotfiles)
* [dkeg](https://github.com/dkeg/dots)
* [itchny](https://github.com/itchyny/dotfiles)
* [jearbear](https://github.com/jearbear/dotfiles)
* [jschx](https://gitlab.com/jschx/etc)
* [mhinz](https://github.com/mhinz/dotfiles)
* [onodera](https://github.com/onodera-punpun/dotfiles)
* [sdothum](https://github.com/sdothum/dotfiles)

[info_os]: https://www.archlinux.org/
[info_shell]: https://www.gnu.org/software/bash/
[info_wm]: https://github.com/baskerville/bspwm
[info_term]: https://st.suckless.org/
[info_font]: https://github.com/adobe-fonts/source-code-pro
[font_rendering]: https://gist.github.com/cryzed/e002e7057435f02cc7894b9e748c5671
[info_colors]: https://designsystem.digital.gov/design-tokens/color/system-tokens/
[info_stow]: https://www.gnu.org/software/stow/
