#/usr/bin/env bash

# -----------------------------------------------------------------------------
# Description: Add completion words to bin scripts
# Location: $HOME/.local/bin/bin_completion.bash
# -----------------------------------------------------------------------------

# ipv6
complete -W "up down enable disable" ipv6
# music
complete -W "next pause play prev status stop toggle" music
# volume
complete -W "down max min mute status toggle up" volume
# what
complete -W "date music time volume weather battery network" what
